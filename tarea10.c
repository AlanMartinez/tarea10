#include <stdio.h>

int main(){
    int a, b, c;

    printf( "Introduzca el primer numero: ");
    scanf( "%d", &a );
    printf( "Introduzca el segundo numero: ");
    scanf( "%d", &b );
    printf( "Introduzca el tercer numero: ");
    scanf( "%d", &c );

    if ( a >= b && a >= c )
        printf( "%d es el mayor.\n", a );
    else
        if ( b > c )
            printf( "%d es el mayor.\n", b );
        else
            printf( "%d es el mayor.\n", c );

    return 0;
}
